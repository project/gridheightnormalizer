Grid height normalizer module allows to equalize grid height of
selector. The module will search the element having largest pixel
height and equalize the height with rest of the elements.

Actions to be done:
- Enable the grid height normalizer module
- Add selectors to admin/structure/grid-height/settings textarea which are
  seperated with newline.
  eg: if title of an article wrapped with article-title class, then
  enter ".article-title" to admin settings form.
